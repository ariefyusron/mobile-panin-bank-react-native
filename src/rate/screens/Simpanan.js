import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, ScrollView, Image, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';

class Simpanan extends Component {
  constructor(){
    super()
    this.state = {
      data: [
        {
          image: 'https://cdnx.kincir.com/old/2017/juli/bikin-seru-momen-santai-keluarga-dengan-5-serial-tv-ini/main-1300x500.jpg',
          title: 'Tabungan Panin & Panin Plus',
          detail: 'Tabungan yang memberikan keuntungan',
          star: 5
        },
        {
          image: 'https://cdnx.kincir.com/old/2017/juli/bikin-seru-momen-santai-keluarga-dengan-5-serial-tv-ini/main-1300x500.jpg',
          title: 'Tabungan Panin & Panin Plus',
          detail: 'Tabungan yang memberikan keuntungan',
          star: 5
        },
        {
          image: 'https://cdnx.kincir.com/old/2017/juli/bikin-seru-momen-santai-keluarga-dengan-5-serial-tv-ini/main-1300x500.jpg',
          title: 'Tabungan Panin & Panin Plus',
          detail: 'Tabungan yang memberikan keuntungan',
          star: 5
        },
        {
          image: 'https://cdnx.kincir.com/old/2017/juli/bikin-seru-momen-santai-keluarga-dengan-5-serial-tv-ini/main-1300x500.jpg',
          title: 'Tabungan Panin & Panin Plus',
          detail: 'Tabungan yang memberikan keuntungan',
          star: 4
        },
        {
          image: 'https://cdnx.kincir.com/old/2017/juli/bikin-seru-momen-santai-keluarga-dengan-5-serial-tv-ini/main-1300x500.jpg',
          title: 'Tabungan Panin & Panin Plus',
          detail: 'Tabungan yang memberikan keuntungan',
          star: 5
        },
        {
          image: 'https://cdnx.kincir.com/old/2017/juli/bikin-seru-momen-santai-keluarga-dengan-5-serial-tv-ini/main-1300x500.jpg',
          title: 'Tabungan Panin & Panin Plus',
          detail: 'Tabungan yang memberikan keuntungan',
          star: 5
        },
        {
          image: 'https://cdnx.kincir.com/old/2017/juli/bikin-seru-momen-santai-keluarga-dengan-5-serial-tv-ini/main-1300x500.jpg',
          title: 'Tabungan Panin & Panin Plus',
          detail: 'Tabungan yang memberikan keuntungan',
          star: 5
        },
        {
          image: 'https://cdnx.kincir.com/old/2017/juli/bikin-seru-momen-santai-keluarga-dengan-5-serial-tv-ini/main-1300x500.jpg',
          title: 'Tabungan Panin & Panin Plus',
          detail: 'Tabungan yang memberikan keuntungan',
          star: 4
        }
      ]
    }
  }

  _star(item){
    let star = []
    for (let i = 0; i < item; i++){
      star.push(<Icon key={i} name='star' type='Entypo' style={{color: '#F1D000', fontSize: 12, marginLeft: i===0? 0:3}} />) 
    }
    return star
  }

  _renderItem =({item, index}) => (
    <View style={[styles.containerProduct, index===0? null:{marginTop: 12}]}>
      <Image
        source={{uri: item.image}}
        style={styles.image}
      />
      <View style={{marginLeft: 12}}>
        <Text style={{fontWeight: 'bold'}}>{item.title}</Text>
        <Text style={{fontSize: 12, marginTop: 2}}>{item.detail}</Text>
        <View style={{flexDirection: 'row'}}>
          <Text style={{fontSize: 11}}>Rate star</Text>
          <View style={{flexDirection: 'row', paddingHorizontal: 10, alignItems: 'center'}}>
            {this._star(item.star)}
          </View>
          <View style={{alignItems: 'flex-end', flex: 1}}>
            <TouchableOpacity>
              <Text style={{fontSize: 11, color: '#3498db'}}>See more</Text>
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity style={styles.button}>
          <Text style={styles.textButton}>APPLY</Text>
        </TouchableOpacity>
      </View>
    </View>
  )

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.content}>
          <View style={styles.header}>
            <Text style={{fontWeight: 'bold'}}>Products</Text>
            <View style={styles.right}>
              <Text style={{fontSize: 11, color: '#8E8E8E', marginRight: 5}}>Sort by</Text>
              <Icon name='sort-amount-asc' type='FontAwesome' style={{fontSize: 14, color: '#8E8E8E'}} />
            </View>
          </View>
          <FlatList
            style={styles.flatList}
            data={this.state.data}
            keyExtractor={(item,index) => index.toString()}
            renderItem={this._renderItem}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F4F4F4',
    flex: 1
  },
  content: {
    backgroundColor: '#fff',
    marginTop: 12
  },
  header: {
    borderBottomWidth: 1.5,
    borderBottomColor: '#F4F4F4',
    flexDirection: 'row',
    paddingHorizontal: 8,
    paddingTop: 6,
    paddingBottom: 4
  },
  right: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    flex: 1
  },
  flatList: {
    padding: 8
  },
  image: {
    width: 90,
    height: 80,
    borderRadius: 3
  },
  containerProduct: {
    flexDirection: 'row'
  },
  button: {
    backgroundColor: '#FF1D1D',
    borderRadius: 5,
    width: 70,
    height: 20,
    justifyContent: 'center',
    marginTop: 4
  },
  textButton: {
    color: '#fff',
    fontSize: 10,
    textAlign: 'center'
  }
})

export default Simpanan;