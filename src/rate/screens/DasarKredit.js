import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

class DasarKredit extends Component {

  render() {
    return (
      <View style={styles.container}>
        <Text>DasarKredit</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F4F4F4',
    flex: 1
  },
})

export default DasarKredit;