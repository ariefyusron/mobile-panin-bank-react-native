import React, { Component } from 'react';
import { ScrollView } from 'react-native';

import Banner from '../../components/Banner';
import Simpanan from '../../../rate/screens/Simpanan';
import NewsEvents from '../../components/NewsEvents';

class ProductSavings extends Component {

  render() {
    return (
      <ScrollView style={{backgroundColor: '#F4F4F4'}}>
        <Banner />
        <Simpanan />
        <NewsEvents />
      </ScrollView>
    );
  }
}

export default ProductSavings;