import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

class CompareSavings extends Component {

  render() {
    return (
      <View style={styles.container}>
        <Text>CompareSavings</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F4F4F4',
    flex: 1
  },
})

export default CompareSavings;