import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, StatusBar } from 'react-native';

import Header from '../components/Header';
import Carousel from '../components/Carousel';
import Menu from '../components/Menu';
import MenuProfile from '../components/MenuProfile';
import MenuCurrencyRate from '../components/MenuCurrencyRate';

class HomeScreen extends Component {
  
  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor='#1874b2' barStyle='light-content' />
        <Header />
        <ScrollView>
          <Carousel />
          <Menu navigation={this.props.navigation} />
          <MenuProfile navigation={this.props.navigation} />
          <MenuCurrencyRate />
        </ScrollView>
      </View>      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F4F4F4',
    flex: 1
  }
})

export default HomeScreen;