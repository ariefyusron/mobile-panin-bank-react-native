import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';

const listItem = [
  {
    title: 'Surat Edaran - Ketentuan Penghitungan KPR.pdf',
    time: '21/1/2019 3:35 PM'
  },
  {
    title: 'Surat Edaran - Ketentuan Penghitungan KPR.pdf',
    time: '21/1/2019 3:35 PM'
  },
  {
    title: 'Surat Edaran - Ketentuan Penghitungan KPR.pdf',
    time: '21/1/2019 3:35 PM'
  },
  {
    title: 'Surat Edaran - Ketentuan Penghitungan KPR.pdf',
    time: '21/1/2019 3:35 PM'
  }
]

class PaninPolicy extends Component {

  _renderItem = ({item}) => (
    <View style={styles.containerItem}>
      <View style={{flex: 1}}>
        <View style={styles.icon}>
          <Text style={styles.textIcon}>PDF</Text>
        </View>
      </View>
      <View style={{flex: 7}}>
        <Text style={{fontSize: 14, fontWeight: 'bold'}}>{item.title}</Text>
        <Text style={{fontSize: 12, marginTop: 2}}>{item.time}</Text>
      </View>
    </View>
  )

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={listItem}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this._renderItem}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1
  },
  containerItem: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1.5,
    borderBottomColor: '#DEDEDE',
    padding: 8
  },
  icon: {
    borderRadius: 5,
    backgroundColor: '#3498db',
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textIcon: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#fff'
  }
})

export default PaninPolicy;