import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Text } from 'react-native';
import { Icon } from 'native-base';

const listMenu = [
  {
    name: 'screen-smartphone',
    type: 'SimpleLineIcons',
    label: 'Savings',
    navigate: 'Savings'
  },
  {
    name: 'money',
    type: 'FontAwesome',
    label: 'Deposito',
    navigate: 'Deposito'
  },
  {
    name: 'file-invoice-dollar',
    type: 'FontAwesome5',
    label: 'Personal Loan',
    navigate: 'PersonalLoan'
  },
  {
    name: 'screen-smartphone',
    type: 'SimpleLineIcons',
    label: 'Home Loan',
    navigate: 'HomeLoan'
  },
  {
    name: 'screen-smartphone',
    type: 'SimpleLineIcons',
    label: 'Car Loan',
    navigate: 'CarLoan'
  },
  {
    name: 'money',
    type: 'FontAwesome',
    label: 'Insurance Investment',
    navigate: 'Insurance'
  },
  {
    name: 'file-invoice-dollar',
    type: 'FontAwesome5',
    label: 'Credit Card',
    navigate: 'CreditCard'
  },
  {
    name: 'screen-smartphone',
    type: 'SimpleLineIcons',
    label: 'Others',
    navigate: 'Others'
  }
]

const Menu = ({navigation}) => {

  return (
    <View style={styles.container}>
      <FlatList
        data={listMenu}
        keyExtractor={(item, index) => index.toString()}
        numColumns={4}
        renderItem={({item}) => (
          <TouchableOpacity style={styles.containerMenu} onPress={() => navigation.navigate(item.navigate)}>
            <View style={styles.containerIcon}>
              <Icon name={item.name} type={item.type} style={styles.icon} />
            </View>
            <Text style={styles.label}>{item.label}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingVertical: 12,
    marginTop: 10
  },
  containerMenu: {
    marginRight: 6,
    marginLeft: 6,
    marginTop: 8,
    marginBottom: 8,
    width: 80,
    alignItems: 'center'
  },
  containerIcon: {
    backgroundColor: '#3498db',
    borderRadius: 30,
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    color: '#fff',
    fontSize: 25
  },
  label: {
    textAlign: 'center',
    fontSize: 12
  }
})

export default Menu;