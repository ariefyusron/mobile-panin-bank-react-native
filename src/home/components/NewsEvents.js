import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, Image } from 'react-native';

const listItem = [
  {
    image: 'https://i.ytimg.com/vi/zzFLjz2sQvU/maxresdefault.jpg',
    detail: 'Halo Palembang! 3 Mercedes Benz GLC dan 88 unit Honda PCX akan diundipada malam ini',
    type: 'Events'
  },
  {
    image: 'https://i.ytimg.com/vi/zzFLjz2sQvU/maxresdefault.jpg',
    detail: 'Halo Palembang! 3 Mercedes Benz GLC dan 88 unit Honda PCX akan diundipada malam ini',
    type: 'News'
  }
]

class NewsEvents extends Component {

  _renderItem = ({item, index}) => (
    <View style={[styles.containerItem, index===0? {marginLeft: 0}:null]}>
      <Image
        source={{uri: item.image}}
        style={styles.image}
      />
      <Text style={{color: '#FF1D1D', fontSize: 10, marginTop: 6}}>{item.type}</Text>
      <Text style={{fontSize: 10, fontWeight: 'bold'}}>{item.detail}</Text>
    </View>
  )

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.contentHeader}>
          <Text style={{fontWeight: 'bold'}}>News & Events <Text style={{color: '#3498db', fontSize: 10, fontWeight: 'normal'}}>See all</Text></Text>
        </View>
        <FlatList
          data={listItem}
          horizontal={true}
          style={styles.flatList}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this._renderItem}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    marginTop: 12
  },
  contentHeader: {
    paddingHorizontal: 8,
    padding: 6,
    borderBottomWidth: 1.5,
    borderBottomColor: '#F4F4F4',
  },
  flatList: {
    padding: 8
  },
  containerItem: {
    width: 165,
    marginLeft: 10
  },
  image: {
    width: 125,
    height: 125
  }
})

export default NewsEvents;