import React from 'react';
import { View, StyleSheet, FlatList, Text, Image } from 'react-native';

const listCurrency = [
  {
    currency: 'AUD',
    buy: 10483,
    sell: 10523
  },
  {
    currency: 'AUD',
    buy: 10483,
    sell: 10523
  },
  {
    currency: 'AUD',
    buy: 10483,
    sell: 10523
  },
  {
    currency: 'AUD',
    buy: 10483,
    sell: 10523
  },
  {
    currency: 'AUD',
    buy: 10483,
    sell: 10523
  },
  {
    currency: 'AUD',
    buy: 10483,
    sell: 10523
  },
  {
    currency: 'AUD',
    buy: 10483,
    sell: 10523
  },
  {
    currency: 'AUD',
    buy: 10483,
    sell: 10523
  },
  {
    currency: 'AUD',
    buy: 10483,
    sell: 10523
  },
  {
    currency: 'AUD',
    buy: 10483,
    sell: 10523
  },
  {
    currency: 'AUD',
    buy: 10483,
    sell: 10523
  },
  {
    currency: 'AUD',
    buy: 10483,
    sell: 10523
  },
  {
    currency: 'AUD',
    buy: 10483,
    sell: 10523
  },
  {
    currency: 'AUD',
    buy: 10483,
    sell: 10523
  },
  {
    currency: 'AUD',
    buy: 10483,
    sell: 10523
  }
]

const MenuCurrencyRate = ({navigation}) => {

  return (
    <View style={styles.container}>
      <View style={styles.contentHeader}>
        <Text style={styles.title}>Currency Rate <Text style={styles.subTitle}>26 Nov 2018 15:00</Text></Text>
        <View style={styles.right}>
          <Text style={[styles.title, styles.titleRight]}>IDR</Text>
          <Image
            source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Flag_of_Indonesia.svg/2000px-Flag_of_Indonesia.svg.png'}}
            style={styles.image}
          />
          <Image
            source={{uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRmEopU1hB5zfYC3gh3mthNFcWsjgV-nWnuguZuvjpEI2HRMaTZ'}}
            style={styles.image}
          />
        </View>
      </View>
      <View style={styles.content}>
        <View style={styles.containerList}>
          <FlatList
            data={listCurrency}
            keyExtractor={(item, index) => index.toString()}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            renderItem={({item, index}) => (
              <View style={[styles.containerListItem, index===0? {marginLeft: 0}:null]}>
                <Text style={styles.labelCurrency}>{item.currency}</Text>
                <Text style={styles.labelBuySell}>Beli</Text>
                <Text style={styles.nominal}>{item.buy}</Text>
                <Text style={styles.labelBuySell}>Jual</Text>
                <Text style={styles.nominal}>{item.sell}</Text>
              </View>
            )}
          />
        </View>
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 12
  },
  contentHeader: {
    paddingHorizontal: 8,
    paddingBottom: 8,
    borderBottomWidth: 1.5,
    borderBottomColor: '#F4F4F4',
    flexDirection: 'row'
  },
  image: {
    width: 20,
    height: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#F4F4F4',
    marginLeft: 4
  },
  right: {
    justifyContent: 'flex-end',
    flex: 1,
    flexDirection: 'row'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 14
  },
  titleRight: {
    fontSize: 11,
    marginRight: 4
  },
  subTitle: {
    fontSize: 10,
    fontWeight: 'normal'
  },
  content: {
    paddingTop: 6,
    paddingHorizontal: 8
  },
  containerList: {
    paddingHorizontal: 8,
    padding: 6,
    backgroundColor: '#F4F4F4'
  },
  containerListItem: {
    backgroundColor: '#fff',
    marginLeft: 6,
    paddingVertical: 10,
    paddingLeft: 8,
    width: 90
  },
  labelCurrency: {
    fontWeight: 'bold',
    fontSize: 12,
    marginBottom: 3,
    color: '#3498db'
  },
  labelBuySell: {
    fontSize: 10,
    marginTop: 4,
    color: '#3498db'
  },
  nominal: {
    fontSize: 11,
    fontWeight: 'bold'
  }
})

export default MenuCurrencyRate;