import React, { Component } from 'react';
import { View, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { Icon } from 'native-base';

class Header extends Component {

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <TouchableOpacity>
            <Icon name='barcode-scan' type='MaterialCommunityIcons' style={styles.icon} />
          </TouchableOpacity>
        </View>
        <View style={styles.center}>
          <View style={styles.containerSearch}>
            <Icon name='md-search' type='Ionicons' style={styles.iconSearch} />
            <TextInput
              placeholder='Cari Produk Panin'
              style={styles.inputSeacrh}
            />
          </View>
        </View>
        <View style={styles.col}>
          <TouchableOpacity>
            <Icon name='heart' type='AntDesign' style={[styles.icon, {fontSize: 20}]} />
          </TouchableOpacity>
        </View>
        <View style={styles.col}>
          <TouchableOpacity>
            <Icon name='ios-notifications' type='Ionicons' style={[styles.icon, {fontSize: 25}]} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#3498db',
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 15
  },
  col: {
    width: '10%'
  },
  center: {
    width: '70%',
    paddingHorizontal: 10,
    justifyContent: 'center'
  },
  icon: {
    color: '#fff',
    fontSize: 30
  },
  containerSearch: {
    backgroundColor: '#fff',
    borderRadius: 25,
    flexDirection: 'row'
  },
  iconSearch: {
    fontSize: 25, alignSelf: 'center',
    marginLeft: 10,
    color: '#989898'
  },
  inputSeacrh: {
    paddingVertical: 2,
    paddingHorizontal: 7,
    fontSize: 12
  }
})

export default Header;