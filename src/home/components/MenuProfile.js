import React from 'react';
import { View, TouchableOpacity, StyleSheet, FlatList, Text } from 'react-native';
import { Icon } from 'native-base';

const listMenu = [
  {
    name: 'screen-smartphone',
    type: 'SimpleLineIcons',
    label: 'My Performance',
    navigate: ''
  },
  {
    name: 'money',
    type: 'FontAwesome',
    label: 'Panin Policy',
    navigate: 'PaninPolicy'
  },
  {
    name: 'file-invoice-dollar',
    type: 'FontAwesome5',
    label: 'Panin in Action',
    navigate: ''
  },
  {
    name: 'screen-smartphone',
    type: 'SimpleLineIcons',
    label: 'Panin Others',
    navigate: ''
  }
]

const MenuProfile = ({navigation}) => {

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Hello, Sugi</Text>
      <FlatList
        data={listMenu}
        keyExtractor={(item, index) => index.toString()}
        numColumns={4}
        renderItem={({item}) => (
          <TouchableOpacity style={styles.containerMenu} onPress={() => navigation.navigate(item.navigate)}>
            <View style={styles.containerIcon}>
              <Icon name={item.name} type={item.type} style={styles.icon} />
            </View>
            <Text style={styles.label}>{item.label}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 12
  },
  title: {
    alignSelf: 'flex-start',
    paddingHorizontal: 8,
    fontWeight: 'bold'
  },
  containerMenu: {
    marginRight: 6,
    marginLeft: 6,
    marginTop: 8,
    marginBottom: 8,
    width: 80,
    alignItems: 'center'
  },
  containerIcon: {
    backgroundColor: '#3498db',
    borderRadius: 30,
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    color: '#fff',
    fontSize: 25
  },
  label: {
    textAlign: 'center',
    fontSize: 12
  }
})

export default MenuProfile;