import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet, TouchableOpacity } from 'react-native';
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';

class JumbotronCarousel extends Component {
  constructor(){
    super()
    this.state = {
      data: [
        {image: 'https://images-loyalty.ovo.id/public/deal/15/83/l/10811.jpg?ver=1'},
        {image: 'https://nomortelepon.id/wp-content/uploads/2016/08/bank-panin.jpg'},
        {image: 'https://3.bp.blogspot.com/-oaloPOfKheE/WmKfXkmXJYI/AAAAAAAAIsE/0ud8CtBHts0bhD2Zy59xDofv4-0Df8taQCLcBGAs/w1200-h630-p-k-no-nu/Lowongan%2BKerja%2BPT%2BBank%2BPanin%2BTbk%2BMalang%2BTahun%2B2018.jpg'},
        {image: 'https://3.bp.blogspot.com/-oaloPOfKheE/WmKfXkmXJYI/AAAAAAAAIsE/0ud8CtBHts0bhD2Zy59xDofv4-0Df8taQCLcBGAs/w1200-h630-p-k-no-nu/Lowongan%2BKerja%2BPT%2BBank%2BPanin%2BTbk%2BMalang%2BTahun%2B2018.jpg'}
      ],
      activeSlide: 0,
      width: Dimensions.get('window').width
    }
  }

  _renderItem = ({item, index}, parallaxProps) => (
    <ParallaxImage
        source={{ uri: item.image }}
        dimensions={{width: this.state.width/1.5, height: 230}}
        {...parallaxProps}
    />
  )

  render() {
    return (
      <View style={styles.container}>
        <Carousel
          data={this.state.data}
          renderItem={this._renderItem}
          sliderWidth={this.state.width}
          itemWidth={this.state.width}
          onSnapToItem={(index) => this.setState({ activeSlide: index })}
          hasParallaxImages={true}
        />
        <View style={styles.footer}>
          <View style={styles.left}>
            {this.state.data.map((item, index) => 
              <View key={index} style={[styles.pagination, index===0? {marginLeft: 0}:null, index===this.state.activeSlide? {backgroundColor: 'orange'}:null]}></View>
            )}
          </View>
          <View style={styles.right}>
            <TouchableOpacity>
                <Text style={styles.textButton}>See all promo</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff'
  },
  footer: {
    flexDirection: 'row',
    paddingVertical: 6,
    paddingHorizontal: 10
  },
  left: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  pagination: {
    backgroundColor: '#1f84c7',
    height: 10,
    width: 10,
    borderRadius: 5,
    marginLeft: 5
  },
  right: {
    flex: 1,
    alignItems: 'flex-end'
  },
  textButton: {
    color: '#3498db',
    fontSize: 12
  }
})

export default JumbotronCarousel;