import React, { Component } from 'react';
import { View, Image, StyleSheet, FlatList } from 'react-native';

const listItem = [
  'https://i.ytimg.com/vi/zzFLjz2sQvU/maxresdefault.jpg',
  'http://www.panin.co.id/doc//CMSUpload/Images/Banner-web-ori11-2014-930x275-ind-full635476581879373716.jpg',
  'http://www.panin.co.id/doc//CMSUpload/Images/Banner-web-ori11-2014-930x275-ind-full635476581879373716.jpg'
]

class Banner extends Component {

  _renderItem = ({item, index}) => (
    <Image
      source={{uri: item}}
      style={[styles.image, index===0? {height: 200}:null]}
    />
  )

  render() {
    return (
      <View style={{backgroundColor: '#fff'}}>
        <FlatList
          data={listItem}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this._renderItem}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff'
  },
  image: {
    marginBottom: 8,
    width: '100%',
    height: 100
  }
})

export default Banner;