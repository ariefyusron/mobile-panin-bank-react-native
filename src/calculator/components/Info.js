import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';

class Info extends Component {

  render() {
    return (
      <View style={styles.content}>
        <Text style={{fontWeight: 'bold', textAlign: 'center'}}>{this.props.title}</Text>
        <View style={styles.containerNominal}>
          <Text style={{fontWeight: 'bold', textAlign: 'center', fontSize: 25}}>{this.props.nominal}</Text>
          <Text style={{fontWeight: 'bold', width: 40, marginLeft: 15}}>{this.props.detail}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    marginTop: 15,
    padding: 20,
    borderRadius: 10,
    backgroundColor: '#fff'
  },
  containerNominal: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15
  }
})

export default Info;