import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';

class KursPanin extends Component {

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.scrollView}>
          <View style={styles.content}>
            <View style={styles.row}>
              <View style={styles.col}>
                <Text style={styles.text}>Nilai Tukar Mata Uang Terhadap</Text>
              </View>
              <View style={styles.col}>
                <Text style={{marginLeft: 20}}>IDR</Text>
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.col}>
                <Text style={styles.text}>Nilai Tukar Mata Uang Terhadap</Text>
              </View>
              <View style={styles.col}>
                <Text style={{marginLeft: 20}}>IDR</Text>
              </View>
            </View>
            <View style={[styles.row, {marginBottom: 0}]}>
              <Text style={[styles.text, {color: '#4A4A4A'}]}>Today, 26 Nov 2018 15:00 WIB</Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F4F4F4',
    flex: 1
  },
  scrollView: {
    paddingHorizontal: 15
  },
  content: {
    backgroundColor: '#fff',
    borderRadius: 10,
    padding: 20,
    marginTop: 15
  },
  row: {
    flexDirection: 'row',
    marginBottom: 15
  },
  col: {
    flex: 1
  },
  text: {
    fontSize: 12
  }
})

export default KursPanin;