import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, TextInput } from 'react-native';

import Info from '../components/Info';

class CicilanKartuKredit extends Component {

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.scrollView}>
          <Info
            title='Angsuran Anda adalah'
            nominal='14,173,333.33'
            detail='Per Bulan'
          />
          <View style={[styles.content, {paddingHorizontal: 20, paddingVertical: 10}]}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={{fontWeight: 'bold', flex: 1}}>Jumlah</Text>
              <TextInput
                value='40.000.000'
                style={styles.labelNominal}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4'
  },
  scrollView: {
    paddingHorizontal: 15
  },
  content: {
    marginTop: 15,
    padding: 20,
    borderRadius: 10,
    backgroundColor: '#fff'
  },
  labelNominal: {
    color: '#3498db', flex: 2,
    borderColor: '#AFAFAF',
    borderWidth: 1,
    height: '70%',
    borderRadius: 10,
    textAlign: 'right',
    paddingHorizontal: 10
  }
})

export default CicilanKartuKredit;