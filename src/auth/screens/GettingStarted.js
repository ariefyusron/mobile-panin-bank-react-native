import React, { Component } from 'react';
import { View, Text, StatusBar } from 'react-native';

class GettingStarted extends Component {

  componentDidMount() {
    this.props.navigation.navigate('Login')
  }

  render() {
    return (
      <View>
        <StatusBar backgroundColor='#1874b2' barStyle='light-content' />
        <Text>GettingStarted</Text>
      </View>
    );
  }
}

export default GettingStarted;