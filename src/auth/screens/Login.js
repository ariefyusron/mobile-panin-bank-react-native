import React, { Component } from 'react';
import { View, Text, TextInput, Image, StyleSheet, TouchableOpacity } from 'react-native';

class Login extends Component {

  handleLogin = () => {
    this.props.navigation.navigate('Profile')
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          source={{uri: 'https://citramaja.com/wp-content/uploads/2018/02/panin-bank.jpg'}}
          style={styles.banner}
        />
        <View style={styles.row}>
          <Text style={{fontWeight: 'bold', fontSize: 18}}>Welcome</Text>
          <Text style={{color: '#939393', fontSize: 12, width: '60%', marginTop: 5}}>Enter your user id and password to get access your account.</Text>
        </View>
        <View style={[styles.row, {marginTop: 12}]}>
          <TouchableOpacity style={styles.button} onPress={this.handleLogin}>
            <Text style={styles.textButton}>Next</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1
  },
  banner: {
    width: '80%',
    height: 100,
    marginTop: 25,
    alignSelf: 'center'
  },
  row: {
    paddingHorizontal: 15
  },
  button: {
    borderRadius: 8,
    backgroundColor: '#3498db',
    height: 35,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textButton: {
    color: '#fff',
    fontSize: 12
  }
})

export default Login;