import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';

class Login extends Component {

  handleLogin = () => {
    this.props.navigation.navigate('HomeScreen')
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.row}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>Hello Sugi,</Text>
          <Text style={{color: '#939393', fontSize: 12, width: '60%', marginTop: 5}}>Please complete for your profile picture</Text>
        </View>
        <View style={[styles.row, {alignItems: 'center'}]}>
          <View style={styles.containerPicture}>
            <Icon name='md-person' type='Ionicons' style={styles.icon} />
            <View style={styles.containerCamera}>
              <Icon name='camera' type='Ionicons' style={styles.iconCamera} />
            </View>
          </View>
        </View>
        <View style={[styles.row, {marginTop: 100}]}>
          <TouchableOpacity style={styles.button} onPress={this.handleLogin}>
            <Text style={styles.textButton}>Next</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1
  },
  row: {
    paddingHorizontal: 15,
    marginTop: 20
  },
  button: {
    borderRadius: 8,
    backgroundColor: '#3498db',
    height: 35,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textButton: {
    color: '#fff',
    fontSize: 12
  },
  containerPicture: {
    backgroundColor: '#C1C1C1',
    width: 230,
    height: 230,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 115
  },
  icon: {
    fontSize: 150,
    color: '#7A7A7A'
  },
  containerCamera: {
    backgroundColor: '#3498db',
    width: 75,
    height: 75,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 40,
    position: 'absolute',
    right: 0,
    bottom: 0
  },
  iconCamera: {
    fontSize: 30,
    color: '#fff'
  }
})

export default Login;