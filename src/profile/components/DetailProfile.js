import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Icon } from 'native-base';

class DetailProfile extends Component {

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerIcon}>
          <Icon name='person' type='Ionicons' style={styles.icon} />
        </View>
        <View style={{justifyContent: 'center', marginLeft: 12}}>
          <Text style={{fontWeight: 'bold'}}>John Alexander</Text>
          <Text style={{fontSize: 11, fontWeight: 'bold', color: '#B7B7B7', marginTop: 4}}>Gold Member</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 12,
    borderBottomColor: '#B7B7B7',
    borderBottomWidth: 0.5
  },
  containerIcon: {
    backgroundColor: '#747474',
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    color: '#fff'
  }
})

export default DetailProfile;