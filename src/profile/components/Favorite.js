import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

class Favorite extends Component {

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={{fontWeight: 'bold'}}>Favorit Saya</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff'
  },
  header: {
    borderBottomColor: '#B7B7B7',
    borderBottomWidth: 0.5,
    padding: 8
  }
})

export default Favorite;