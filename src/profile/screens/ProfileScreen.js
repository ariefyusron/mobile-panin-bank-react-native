import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import DetailProfile from '../components/DetailProfile';
import Favorite from '../components/Favorite';

class ProfileScreen extends Component {

  render() {
    return (
      <View style={styles.container}>
        <DetailProfile />
        <Favorite />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff'
  }
})

export default ProfileScreen;