import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { Icon } from 'native-base';
import { createAppContainer, createMaterialTopTabNavigator, createBottomTabNavigator, createStackNavigator } from 'react-navigation';

//Basic
import TabNavigator from '../components/TabNavigator';
import FooterTab from '../components/FooterTab';
//Auth
import GettingStarted from '../../auth/screens/GettingStarted';
import Login from '../../auth/screens/Login';
import PorfileAuth from '../../auth/screens/Profile';
//Home Screen
import HomeScreen from '../../home/screens/HomeScreen';
import PaninPolicy from '../../home/screens/PaninPolicy';
//Savings
import ProductSavings from '../../home/screens/Savings/ProductSavings';
import CompareSavings from '../../home/screens/Savings/CompareSavings';
import News from '../../home/screens/Savings/News';
//Deposito
import Description from '../../home/screens/Deposito/Description';
import InterestRate from '../../home/screens/Deposito/InterestRate';
import FeeCharges from '../../home/screens/Deposito/FeeCharges';
import TermCondt from '../../home/screens/Deposito/TermCondt';
//Personal Loan
import DescriptionPersonalLoan from '../../home/screens/PersonalLoan/Description';
import InterestRatePersonalLoan from '../../home/screens/PersonalLoan/InterestRate';
import FeeChargesPersonalLoan from '../../home/screens/PersonalLoan/FeeCharges';
import TermCondtPersonalLoan from '../../home/screens/PersonalLoan/TermCondt';
//Home Loan
import DescriptionHomeLoan from '../../home/screens/HomeLoan/Description';
import InterestRateHomeLoan from '../../home/screens/HomeLoan/InterestRate';
import FeeChargesHomeLoan from '../../home/screens/HomeLoan/FeeCharges';
import TermCondtHomeLoan from '../../home/screens/HomeLoan/TermCondt';
//Car Loan
import DescriptionCarLoan from '../../home/screens/CarLoan/Description';
import InterestRateCarLoan from '../../home/screens/CarLoan/InterestRate';
import FeeChargesCarLoan from '../../home/screens/CarLoan/FeeCharges';
import TermCondtCarLoan from '../../home/screens/CarLoan/TermCondt';
//Insurance
import InsuranceProductSavings from '../../home/screens/Insurance/ProductSavings';
import InsuranceCompareSavings from '../../home/screens/Insurance/CompareSavings';
import InsuranceNews from '../../home/screens/Insurance/News';
//Calculator Screen
import KursPanin from '../../calculator/screens/KursPanin';
import Kpr from '../../calculator/screens/Kpr';
import KreditExpress from '../../calculator/screens/KreditExpress';
import CicilanKartuKredit from '../../calculator/screens/CicilanKartuKredit';
//Rate Screen
import Simpanan from '../../rate/screens/Simpanan';
import Pinjaman from '../../rate/screens/Pinjaman';
import DasarKredit from '../../rate/screens/DasarKredit';
//Chat Screen
import ChatScreen from '../../chat/screens/ChatScreen';
//Profile Screen
import ProfileScreen from '../../profile/screens/ProfileScreen';

const topTabHomeScreenSavings = createMaterialTopTabNavigator({
  ProductSavings: {
    screen: ProductSavings,
    navigationOptions: {
      tabBarLabel: 'Product Screen'
    }
  },
  CompareSavings: {
    screen: CompareSavings,
    navigationOptions: {
      tabBarLabel: 'Compare Savings'
    }
  },
  News: {
    screen: News,
    navigationOptions: {
      tabBarLabel: 'News'
    }
  }
},{
  lazy: true,
  backBehavior: 'none',
  tabBarOptions: {
    upperCaseLabel: false,
    labelStyle: {
      color: '#3498db',
      fontSize: 12
    },
    indicatorStyle: {
      backgroundColor: '#3498db'
    },
    tabStyle: {
      height: 35
    },
    style: {
      backgroundColor: '#fff'
    }
  }
})

const topTabHomeScreenDeposito = createMaterialTopTabNavigator({
  Description,
  InterestRate: {
    screen: InterestRate,
    navigationOptions: {
      tabBarLabel: 'Interest Rate'
    }
  },
  FeeCharges: {
    screen: FeeCharges,
    navigationOptions: {
      tabBarLabel: 'Fee & Charges'
    }
  },
  TermCondt: {
    screen: TermCondt,
    navigationOptions: {
      tabBarLabel: 'Term & Condt'
    }
  }
},{
  lazy: true,
  backBehavior: 'none',
  tabBarOptions: {
    upperCaseLabel: false,
    labelStyle: {
      color: '#3498db',
      fontSize: 12
    },
    indicatorStyle: {
      backgroundColor: '#3498db'
    },
    tabStyle: {
      height: 35
    },
    style: {
      backgroundColor: '#fff'
    }
  }
})

const bottomTabHomeScreenDeposito = createBottomTabNavigator({
  topTabHomeScreenDeposito
},{
  tabBarComponent: props => <FooterTab {...props} />
})

const topTabHomeScreenPersonalLoan = createMaterialTopTabNavigator({
  Description: DescriptionPersonalLoan,
  InterestRate: {
    screen: InterestRatePersonalLoan,
    navigationOptions: {
      tabBarLabel: 'Interest Rate'
    }
  },
  FeeCharges: {
    screen: FeeChargesPersonalLoan,
    navigationOptions: {
      tabBarLabel: 'Fee & Charges'
    }
  },
  TermCondt: {
    screen: TermCondtPersonalLoan,
    navigationOptions: {
      tabBarLabel: 'Term & Condt'
    }
  }
},{
  lazy: true,
  backBehavior: 'none',
  tabBarOptions: {
    upperCaseLabel: false,
    labelStyle: {
      color: '#3498db',
      fontSize: 12
    },
    indicatorStyle: {
      backgroundColor: '#3498db'
    },
    tabStyle: {
      height: 35
    },
    style: {
      backgroundColor: '#fff'
    }
  }
})

const bottomTabHomeScreenPersonalLoan = createBottomTabNavigator({
  topTabHomeScreenPersonalLoan
},{
  tabBarComponent: props => <FooterTab {...props} />
})

const topTabHomeScreenHomeLoan = createMaterialTopTabNavigator({
  Description: DescriptionHomeLoan,
  InterestRate: {
    screen: InterestRateHomeLoan,
    navigationOptions: {
      tabBarLabel: 'Interest Rate'
    }
  },
  FeeCharges: {
    screen: FeeChargesHomeLoan,
    navigationOptions: {
      tabBarLabel: 'Fee & Charges'
    }
  },
  TermCondt: {
    screen: TermCondtHomeLoan,
    navigationOptions: {
      tabBarLabel: 'Term & Condt'
    }
  }
},{
  lazy: true,
  backBehavior: 'none',
  tabBarOptions: {
    upperCaseLabel: false,
    labelStyle: {
      color: '#3498db',
      fontSize: 12
    },
    indicatorStyle: {
      backgroundColor: '#3498db'
    },
    tabStyle: {
      height: 35
    },
    style: {
      backgroundColor: '#fff'
    }
  }
})

const bottomTabHomeScreenHomeLoan = createBottomTabNavigator({
  topTabHomeScreenHomeLoan
},{
  tabBarComponent: props => <FooterTab {...props} />
})

const topTabHomeScreenCarLoan = createMaterialTopTabNavigator({
  Description: DescriptionCarLoan,
  InterestRate: {
    screen: InterestRateCarLoan,
    navigationOptions: {
      tabBarLabel: 'Interest Rate'
    }
  },
  FeeCharges: {
    screen: FeeChargesCarLoan,
    navigationOptions: {
      tabBarLabel: 'Fee & Charges'
    }
  },
  TermCondt: {
    screen: TermCondtCarLoan,
    navigationOptions: {
      tabBarLabel: 'Term & Condt'
    }
  }
},{
  lazy: true,
  backBehavior: 'none',
  tabBarOptions: {
    upperCaseLabel: false,
    labelStyle: {
      color: '#3498db',
      fontSize: 12
    },
    indicatorStyle: {
      backgroundColor: '#3498db'
    },
    tabStyle: {
      height: 35
    },
    style: {
      backgroundColor: '#fff'
    }
  }
})

const bottomTabHomeScreenCarLoan = createBottomTabNavigator({
  topTabHomeScreenCarLoan
},{
  tabBarComponent: props => <FooterTab {...props} />
})

const topTabHomeScreenInsurance = createMaterialTopTabNavigator({
  InsuranceProductSavings: {
    screen: InsuranceProductSavings,
    navigationOptions: {
      tabBarLabel: 'Product Savings'
    }
  },
  InsuranceCompareSavings: {
    screen: InsuranceCompareSavings,
    navigationOptions: {
      tabBarLabel: 'Compare Savings'
    }
  },
  InsuranceNews: {
    screen: InsuranceNews,
    navigationOptions: {
      tabBarLabel: 'News'
    }
  }
},{
  lazy: true,
  backBehavior: 'none',
  tabBarOptions: {
    upperCaseLabel: false,
    labelStyle: {
      color: '#3498db',
      fontSize: 12
    },
    indicatorStyle: {
      backgroundColor: '#3498db'
    },
    tabStyle: {
      height: 35
    },
    style: {
      backgroundColor: '#fff'
    }
  }
})

const homeScreen = createStackNavigator({
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: {
      header: null
    }
  },
  Savings: {
    screen:topTabHomeScreenSavings,
    navigationOptions:({navigation}) => ({
      headerRight: (
        <View style={{flexDirection: 'row', marginRight: 10}}>
          <Icon name='md-search' type='Ionicons' style={{color: '#fff', marginRight: 20, fontSize: 23}} />
          <Icon name='md-share' type='Ionicons' style={{color: '#fff', fontSize: 23}} />
        </View>
      )
    })
  },
  Insurance: {
    screen: topTabHomeScreenInsurance,
    navigationOptions:({navigation}) => ({
      headerRight: (
        <View style={{flexDirection: 'row', marginRight: 10}}>
          <Icon name='md-search' type='Ionicons' style={{color: '#fff', marginRight: 20, fontSize: 23}} />
          <Icon name='md-share' type='Ionicons' style={{color: '#fff', fontSize: 23}} />
        </View>
      )
    })
  },
  PaninPolicy: {
    screen: PaninPolicy,
    navigationOptions:({navigation}) => ({
      headerRight: (
        <View style={{flexDirection: 'row', marginRight: 10}}>
          <Icon name='md-search' type='Ionicons' style={{color: '#fff', fontSize: 23}} />
        </View>
      )
    })
  }
},{
  defaultNavigationOptions: ({navigation}) => ({
    headerStyle: {
      backgroundColor: '#3498db',
      height: 50
    },
    headerTitle: () => {
      const {routeName} = navigation.state
      let title
      if(routeName==='Savings'){
        title = 'Savings'
      } else if(routeName==='Insurance') {
        title = 'Insurance & Investment'
      } else {
        title = 'Panin Policy'
      }

      return <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>{title}</Text>
    },
    headerLeft: (
      <TouchableOpacity style={{marginLeft: 10}} onPress={() => navigation.navigate('HomeScreen')}>
        <Icon name='md-arrow-back' type='Ionicons' style={{color: '#fff'}} />
      </TouchableOpacity>
    )
  })
})

const topTabCalculatorScreen = createMaterialTopTabNavigator({
  KursPanin: {
    screen: KursPanin,
    navigationOptions: {
      tabBarLabel: 'Kurs Panin'
    }
  },
  KPR: Kpr,
  KreditExpress: {
    screen: KreditExpress,
    navigationOptions: {
      tabBarLabel: 'Kredit Express'
    }
  },
  CicilanKartuKredit: {
    screen: CicilanKartuKredit,
    navigationOptions: {
      tabBarLabel: 'Cicilan Kartu Kredit'
    }
  }
},{
  lazy: true,
  backBehavior: 'none',
  tabBarOptions: {
    upperCaseLabel: false,
    labelStyle: {
      color: '#3498db',
      fontSize: 12
    },
    indicatorStyle: {
      backgroundColor: '#3498db'
    },
    tabStyle: {
      height: 35
    },
    style: {
      backgroundColor: '#fff'
    }
  }
})

const CalculatorScreen = createStackNavigator({
  topTabCalculatorScreen
},{
  defaultNavigationOptions: ({navigation}) => ({
    headerStyle: {
      backgroundColor: '#3498db',
      height: 50
    },
    headerTitle: <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>Calculator</Text>,
    headerLeft: (
      <TouchableOpacity style={{marginLeft: 10}} onPress={() => navigation.navigate('HomeScreen')}>
        <Icon name='md-arrow-back' type='Ionicons' style={{color: '#fff'}} />
      </TouchableOpacity>
    ),
  })
})

const topTabRateScreen = createMaterialTopTabNavigator({
  Simpanan,
  Pinjaman,
  DasarKredit: {
    screen: DasarKredit,
    navigationOptions: {
      tabBarLabel: 'Dasar Kredit'
    }
  }
},{
  lazy: true,
  backBehavior: 'none',
  tabBarOptions: {
    upperCaseLabel: false,
    labelStyle: {
      color: '#3498db',
      fontSize: 12
    },
    indicatorStyle: {
      backgroundColor: '#3498db'
    },
    tabStyle: {
      height: 35
    },
    style: {
      backgroundColor: '#fff'
    }
  }
})

const RateScreen = createStackNavigator({
  topTabRateScreen
},{
  defaultNavigationOptions: ({navigation}) => ({
    headerStyle: {
      backgroundColor: '#3498db',
      height: 50
    },
    headerTitle: <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>Interest Rate</Text>,
    headerLeft: (
      <TouchableOpacity style={{marginLeft: 10}} onPress={() => navigation.navigate('HomeScreen')}>
        <Icon name='md-arrow-back' type='Ionicons' style={{color: '#fff'}} />
      </TouchableOpacity>
    ),
    headerRight: (
      <View style={{flexDirection: 'row', marginRight: 10}}>
        <Icon name='md-search' type='Ionicons' style={{color: '#fff', marginRight: 20, fontSize: 23}} />
        <Icon name='md-share' type='Ionicons' style={{color: '#fff', fontSize: 23}} />
      </View>
    )
  })
})

const chatScreen = createStackNavigator({
  ChatScreen
},{
  defaultNavigationOptions: ({navigation}) => ({
    headerStyle: {
      backgroundColor: '#3498db',
      height: 50
    },
    headerTitle: <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>Live Chat</Text>,
    headerLeft: (
      <TouchableOpacity style={{marginLeft: 10}} onPress={() => navigation.navigate('HomeScreen')}>
        <Icon name='md-arrow-back' type='Ionicons' style={{color: '#fff'}} />
      </TouchableOpacity>
    )
  })
})

const profileScreen = createStackNavigator({
  ProfileScreen
},{
  defaultNavigationOptions: ({navigation}) => ({
    headerStyle: {
      backgroundColor: '#3498db',
      height: 50
    },
    headerTitle: <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>Account</Text>,
    headerLeft: (
      <TouchableOpacity style={{marginLeft: 10}} onPress={() => navigation.navigate('HomeScreen')}>
        <Icon name='md-arrow-back' type='Ionicons' style={{color: '#fff'}} />
      </TouchableOpacity>
    ),
  })
})

const bottomTabNavigator = createBottomTabNavigator({
  HomeScreen: homeScreen,
  CalculatorScreen,
  RateScreen,
  ChatScreen: chatScreen,
  ProfileScreen: profileScreen
},{
  tabBarComponent: props => <TabNavigator {...props} />
})

const appNavigator = createStackNavigator({
  GettingStarted: {
    screen: GettingStarted,
    navigationOptions: {
      header: null
    }
  },
  Login: {
    screen: Login,
    navigationOptions: ({navigation}) => ({
      headerStyle: {
        backgroundColor: '#3498db',
        height: 50
      },
      headerTitle: <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>Login</Text>,
      headerLeft: (
        <TouchableOpacity style={{marginLeft: 10}} onPress={() => navigation.goBack()}>
          <Icon name='md-arrow-back' type='Ionicons' style={{color: '#fff'}} />
        </TouchableOpacity>
      ),
      headerRight: null
    })
  },
  Profile: {
    screen: PorfileAuth,
    navigationOptions: ({navigation}) => ({
      headerStyle: {
        backgroundColor: '#3498db',
        height: 50
      },
      headerTitle: <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>Profile</Text>,
      headerLeft: (
        <TouchableOpacity style={{marginLeft: 10}} onPress={() => navigation.goBack()}>
          <Icon name='md-arrow-back' type='Ionicons' style={{color: '#fff'}} />
        </TouchableOpacity>
      ),
      headerRight: null
    })
  },
  bottomTabNavigator: {
    screen: bottomTabNavigator,
    navigationOptions: {
      header: null
    }
  },
  Deposito: bottomTabHomeScreenDeposito,
  PersonalLoan: bottomTabHomeScreenPersonalLoan,
  HomeLoan: bottomTabHomeScreenHomeLoan,
  CarLoan: bottomTabHomeScreenCarLoan
},{
  defaultNavigationOptions: ({navigation}) => ({
    headerStyle: {
      backgroundColor: '#3498db',
      height: 50
    },
    headerTitle: () => {
      const {routeName} = navigation.state
      let title
      if(routeName==='Deposito'){
        title = 'Deposito'
      } else if(routeName==='PersonalLoan') {
        title = 'Kredit Express'
      } else if(routeName==='HomeLoan') {
        title = 'KPR'
      } else {
        title = 'KPM'
      }
      return <Text style={{color: '#fff', fontSize: 20, fontWeight: 'bold'}}>{title}</Text>
    },
    headerLeft: (
      <TouchableOpacity style={{marginLeft: 10}} onPress={() => navigation.navigate('HomeScreen')}>
        <Icon name='md-arrow-back' type='Ionicons' style={{color: '#fff'}} />
      </TouchableOpacity>
    ),
    headerRight: (
      <View style={{flexDirection: 'row', marginRight: 10}}>
        <Icon name='md-search' type='Ionicons' style={{color: '#fff', marginRight: 20, fontSize: 23}} />
        <Icon name='md-share' type='Ionicons' style={{color: '#fff', fontSize: 23}} />
      </View>
    )
  })
})

export default createAppContainer(appNavigator);