import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';

const listTab = [
  {
    label: 'Apply Now',
    name: 'clipboard-pencil',
    type: 'Foundation',
    navigate: 'apply'
  },
  {
    label: 'Calculator',
    name: 'calculator',
    type: 'Entypo',
    navigate: 'CalculatorScreen'
  },
  {
    label: 'Rate',
    name: 'area-graph',
    type: 'Entypo',
    navigate: 'RateScreen'
  },
  {
    label: 'Chat',
    name: 'ios-chatbubbles',
    type: 'Ionicons',
    navigate: 'ChatScreen'
  },
  {
    label: 'Profile',
    name: 'md-person',
    type: 'Ionicons',
    navigate: 'ProfileScreen'
  }
]

const TabNavigator = ({navigation}) => {
  handleNavigate = (page) => {
    if(page==='apply'){
      alert('Apply Now')
    } else {
      navigation.navigate(page)
    }
  }
  
  return (
    <View style={styles.container}>
      {listTab.map((item, index) => (
        <TouchableOpacity key={index} style={styles.containerItemTab} onPress={() => handleNavigate(item.navigate)}>
          <View style={styles.containerIcon}>
            <Icon name={item.name} type={item.type} style={styles.icon} />
          </View>
          <Text style={styles.label}>{item.label}</Text>
        </TouchableOpacity>
      ))}   
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#3498db',
    flexDirection: 'row',
    paddingVertical: 7
  },
  containerItemTab: {
    alignItems: 'center',
    flex: 1
  },
  containerIcon: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    color: '#fff',
    fontSize: 23
  },
  label: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 10,
    marginTop: 3
  }
})

export default TabNavigator;