import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';

class FooterTab extends Component {

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.containerItemTab}>
          <View style={styles.containerIcon}>
            <Icon name='ios-chatbubbles' type='Ionicons' style={styles.icon} />
          </View>
          <Text style={styles.label}>Chat</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.containerItemTab}>
          <View style={styles.containerIcon}>
            <Icon name='calculator' type='Entypo' style={styles.icon} />
          </View>
          <Text style={styles.label}>Calculator</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.containerItemTab, {backgroundColor: '#FF1D1D', flex: 1.5}]}>
          <View style={styles.containerIcon}>
            <Icon name='clipboard-pencil' type='Foundation' style={[styles.icon, {color: '#fff'}]} />
          </View>
          <Text style={[styles.label, {color: '#fff'}]}>Apply</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.containerItemTab, {backgroundColor: '#3498db', flex: 1.5}]}>
          <View style={styles.containerIcon}>
            <Icon name='ios-call' type='Ionicons' style={[styles.icon, {color: '#fff'}]} />
          </View>
          <Text style={[styles.label, {color: '#fff'}]}>Call Panin</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flexDirection: 'row'
  },
  containerItemTab: {
    alignItems: 'center',
    flex: 1,
    paddingTop: 4
  },
  containerIcon: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    color: '#5F5F5F',
    fontSize: 23
  },
  label: {
    color: '#5F5F5F',
    textAlign: 'center',
    fontSize: 10,
    marginTop: 3
  }
})

export default FooterTab;